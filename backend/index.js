const QRCode = require('qrcode');
const crypto = require('crypto');
const data = require('./data');





const admin = require("firebase-admin");
const serviceAccount = require("./credentials.json");
const os = require('os');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://myawesomeproject666.firebaseio.com"
});

const db = admin.firestore();

const hmacSign = (strData) => crypto.createHmac('sha256', serviceAccount.private_key).update(strData).digest('hex');
const LOCAL_IP = os.networkInterfaces()['Wi-Fi'].filter(e => e.family === 'IPv4')[0].address;
const PORT = 8080;
const LOCAL_ADDR = `${LOCAL_IP}:${PORT}`
const express = require('express')
const app = express();







const createQR = (path, data) => QRCode.toFile(path, JSON.stringify(data, undefined, 2));
const dataUrlQR = (data) => QRCode.toDataURL(JSON.stringify(data, undefined, 2));
const dataUrlRawQR = (data) => QRCode.toDataURL(data);
const uuid = () => crypto.randomBytes(20).toString('hex');



async function createUsers() {
    for (const user of data.users) {
        await admin.auth().createUser({
            uid: user.uid,
            displayName: `${user.name} ${user.surname}`,
            email: user.email,
            phoneNumber: user.phoneNumber,
            emailVerified: false
        });
        await db.collection('users').doc(user.uid).set(user)
    }
    console.log('users added');
}


async function createPackets() {
    // add packets
    for (const [userName, userData] of Object.entries(data.usersByNameDict)) {
        const packetData = {
            packetId: uuid(),
            tsISO: new Date().toISOString(),
            userData
        };
        const packetDataRaw = JSON.stringify(packetData, undefined, 2);
        const packetEntry = {
            packetData,
            packetDataRaw,
            //ce bo to problem daj se packetDataRaw.
            packetHMAC: hmacSign(packetDataRaw)
        };
        const fbEntry = {
            packet: packetEntry,
            qrCode: await dataUrlRawQR(`http://${LOCAL_ADDR}/static/accept.html?packetId=${packetData.packetId}&hmac=${packetEntry.packetHMAC}`),
            deliveredAt: null
        }
        await db.collection('packets').doc(fbEntry.packet.packetData.packetId).set(fbEntry);
    }
    console.log('packets created');
}

app.use('/static', express.static('public'))

let CACHE = null;
app.get('/qr-docs', async (req, res) => {
    const { userId } = req.query;
    let query = db.collection('packets');
    if (userId) {
        query = query.where('packet.packetData.userData.uid', '==', userId);
    }
    const qResult = await query.get();
    const resp = qResult.docs.map(doc => doc.data()).map(doc => ({
        qrCode: doc.qrCode,
        packet: doc.packet,
        userData: doc.packet.packetData.userData,
        deliveredAt: doc.deliveredAt
    }));
    res.status(200).json(resp);
});

app.get('/packet', async (req, res) => {
    const { packetId } = req.query;
    if (!packetId) {
        return res.status(404).json({ message: 'not found' });
    }
    const packet = await db.collection('packets').doc(packetId).get();
    const packetData = packet.data();
    res.status(200).json(packetData);
});

app.get('/packet-accept', async (req, res) => {
    const { packetId, hmac } = req.query;
    if (!packetId) {
        return res.status(404).json({ message: 'not found' });
    }
    await db.collection('packets').doc(packetId).update({ deliveredAt: new Date().toISOString() });
    res.status(200).json({
        message: 'success'
    });
});

async function main() {
    /*
    for (let i = 0; i < 15; i++) {
        await createPackets();
    }*/
    app.listen(PORT);
}

main();