const TADEJA = {
    uid: 'da2a7ee5930b6ded17d5024d8937b6a3df5156fc',
    name: 'Tadeja',
    surname: 'Keršič',
    email: 'tadeja.kersic@inova.si',
    address: 'Bukovec 5',
    postalNumber: '2314',
    phoneNumber: '+38631727991'
};
const ROK = {
    uid: '5c35664dee07bff9e380238948e8326ce1fb1ae8',
    name: 'Rok',
    surname: 'Štumberger',
    email: 'rok.stumberger@inova.si',
    address: 'Hajdina 12a',
    postalNumber: '2288',
    phoneNumber: '+38631727992'
};
const DOMINIK = {
    uid: 'e5ee12aad384248d3c74759ab9021ae000bf9181',
    name: 'Dominik',
    surname: 'Korošec',
    email: 'dominik.korosec@inova.si',
    address: 'Leskovec 58',
    postalNumber: '2331',
    phoneNumber: '+38631727990'
};

const users = [TADEJA, ROK, DOMINIK]
const usersDict = users.reduce((map, user) => ({ ...map, [user.uid]: user }), {});
const usersByNameDict = users.reduce((map, user) => ({ ...map, [user.name.toLowerCase()]: user }), {});

module.exports = {
    users,
    usersDict,
    usersByNameDict
};