
async function makeRequest(endpoint) {
    const response = await fetch(endpoint);
    return response.json();
}


function createPacketDOM(doc, i) {
    const data = doc.packet.packetData;
    const packetDiv = document.createElement("div");
    const delivered = doc.deliveredAt;
    packetDiv.innerHTML = `
        <button id='${doc.packet.packetData.packetId}' ${delivered && `style="background:#FFD541"`} class="collapsible">Paket ${i + 1} - ${delivered || doc.packet.packetData.packetId}</button>
        <div class="content" style="display:none";>
            <img class="qrcodeclass" src="${doc.qrCode}"></img>
        </div>
    `;
    bindToggler(packetDiv);
    return packetDiv
}


function bindToggler(doc) {
    const clicker = doc.firstElementChild;
    clicker.className = 'collapsible';
    clicker.addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });

}

async function rejectPacketFn() {
    window.close()
}

async function acceptPackage(packetId, hmac) {
    await makeRequest(`/packet-accept?packetId=${packetId}&hmac=${hmac}`);
    //window.location.href = '/static/index.html'
    window.close();
}

//accept.html
async function accept() {
    // TA PAGE LAHKO LOADA SAMO POSTA.
    const urlParams = new URLSearchParams(window.location.search);
    const packetId = urlParams.get('packetId');
    const hmac = urlParams.get('hmac');
    const body = await makeRequest(`/packet?packetId=${packetId}&hmac=${hmac}`);
    const { packet } = body
    const { packetData } = packet;
    const { userData } = packetData;
    if (body.deliveredAt) {
        document.body.innerHTML = `
    <div class="potrjenclass">
        <h1> PAKET JE ŽE POTRJEN </h1>
        <h1> Oseba </h1>
        <h3>${userData.name} ${userData.surname}</h3>
        <h3>${userData.address}</h3>
        <h3>${userData.phoneNumber}</h3>
        <h3>${userData.email}</h3>
        <h1> ID Paketa </h1>
        <h3> ${packetData.packetId.substr(0, 20)} </h3>
        <h1> Paket registriran </h1>
        <h3> ${packetData.tsISO} </h3>
        <button type="button" class="buttonOK" onclick="rejectPacketFn()">OK</button> 
    </div>
    `
        return;
    }
    document.body.innerHTML = `
    <div class="potrjenclass">
        <h1> PAKET ČAKA NA VAŠO POTRDITEV </h1>
        <h1> Oseba </h1>
        <h3>${userData.name} ${userData.surname}</h3>
        <h3>${userData.address}</h3>
        <h3>${userData.phoneNumber}</h3>
        <h3>${userData.email}</h3>
        <h1> ID Paketa </h1>
        <h3> ${packetData.packetId.substr(0, 20)} </h3>
        <h1> Paket registriran </h1>
        <h3> ${packetData.tsISO} </h3>
        <button type="button" class="buttonOK" onclick="acceptPackage('${packetId}','${hmac}')" >POTRDI</button> 
        <button class="buttonNOK" type="button" onclick="rejectPacketFn()">ZAVRNI</button> <!-- window close -->
    </div>
    `

    //document.body.innerHTML = JSON.stringify(packetData, null, 2);
}


async function sleep(ms) {
    return new Promise(r => setTimeout(r, ms));
}

//index.html
async function main() {
    const REFRESH_TIME = 1500;
    const urlParams = new URLSearchParams(window.location.search);
    const idParam = urlParams.get('userId') || '';
    let children = 0
    while (1) {
        const documets = await makeRequest(`/qr-docs?userId=${idParam}`);
        if (children !== documets.length) {
            paketi_div.innerHTML = '';
        } else {
            for (const [i, doc] of Object.entries(documets)) {
                const delivered = doc.deliveredAt;
                const btn = document.getElementById(doc.packet.packetData.packetId);
                btn.style.background = delivered && '#FFD541' || '';
                btn.innerHTML = `Paket ${Number(i) + 1} - ${delivered || doc.packet.packetData.packetId}`
            }
            await sleep(REFRESH_TIME);
            continue;
        }
        children = documets.length;
        for (const [i, doc] of Object.entries(documets)) {
            paketi_div.appendChild(createPacketDOM(doc, Number(i)))
        }
        await sleep(REFRESH_TIME);
    }
}