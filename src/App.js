import React from 'react';
import './App.css';
import HooksExamples from './HooksExamples';
import FetchExample from './FetchExample';
import Header from './Header';
import Register from './Register';
import Login from './Login';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { makeStyles } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#FFD541',
        },

    },
    status: {
        danger: 'orange',
    },
});

const useStyles = makeStyles({
    root: {
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
});


function App() {
    const classes = useStyles();
  return (
      <ThemeProvider theme={theme}>
        <div className={classes.root}>
           {/*<AppBar position="static">*/}
                {/*<Toolbar>*/}
                {/*test*/}
                {/*</Toolbar>*/}
           {/*</AppBar>*/}

           {/*<Register/>*/}
           <Login/>

        </div>
      </ThemeProvider>
  );
}

export default App;
