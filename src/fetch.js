import axios from "axios";

const fetchExample = async () => {
    const response = await axios.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1");
    console.log(response);
    return response;
};

export {fetchExample}
