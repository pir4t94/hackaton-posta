import React from 'react';
import useInputState from "./hooks/useInputState";
import TextField from "@material-ui/core/TextField";
import useToggleState from "./hooks/useToggleState";
import Checkbox from "@material-ui/core/Checkbox";

const HooksExamples = () => {
    const [value, handleChange, reset] = useInputState('');
    const [checkboxState, toggle] = useToggleState(false);
    return (
    <div>
     <TextField
        margin="normal"
        value={value}
        onChange={handleChange}
        fullWidth
        autoFocus
      />
     <p>{value}</p>


     <Checkbox
        checked={checkboxState}
        onClick={toggle}
     />
      <p>{checkboxState.toString()}</p>
    </div>

    )
}

export default HooksExamples;