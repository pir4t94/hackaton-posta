import React from 'react';
import {createMuiTheme} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { Input } from '@material-ui/core';


const useStyles = makeStyles({
    root: {
        background: '#FFD541',
        position: 'fixed',
        width: '100%',
        height: '100%',
        textAlign: 'center',
    },
    headline: {
        fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif;',
        marginTop: '150px',
        fontSize: '40px',
    },
    inputName: {
        marginTop: '120px'
    },
    inputPassword: {
        marginTop: '50px'
    }
});


const Register = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <p className={classes.headline}>Register</p>
            <Input placeholder={'Email'} className={classes.inputName}/><br/>
            <Input placeholder={'Password'} className={classes.inputPassword}/><br/>
            <Input placeholder={'Repeat Password'} className={classes.inputPassword}/>
        </div>
    )
};

export default Register;
