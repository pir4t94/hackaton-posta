import React, {useEffect, useState} from 'react';
import {fetchExample} from './fetch';

const FetchExample = ({test}) => {
    const [response, setResponse] = useState('');

    useEffect(()=> {
        fetchExample().then(response => {
                setResponse(response);
            }
        );
    }, []);

    return (
        <>
            <p>{test}</p>
            { response &&
                <p>{response.data.deck_id}</p>
            }
        </>
    )
};

export default FetchExample;
