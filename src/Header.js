import React from 'react';
import Toolbar from "./App";
import AppBar from '@material-ui/core/AppBar';

const Header = () => {
    return (
        <AppBar position="static">
            <Toolbar>
                test
            </Toolbar>
        </AppBar>
    )
};

export default Header;
